class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(value) {
    try {
      if (value.length < 1) {
        throw new Error("Please provide a name for the user");
      }
      this._name = value;
    } catch (e) {
      console.error(`Error trying to assign name! ${e.message}.`);
    }
  }

  set age(value) {
    try {
      if (value < 18) {
        throw new Error("Sorry, you are too young for whatever it is you are trying to engage into..")
      } else if (typeof value !== 'number') {
        throw new Error("Please type in your age correctly");
      }
      this._age = value;
    } catch (e) {
      console.error(`Error trying to assign age! ${e.message}.`);
    }
  }

  set salary(value) {
    try {
      if (value < 800) {
        throw new Error("Come on man, you can't be earning that little!");
      } else if (value > 8000) {
        throw new Error("Come on man, you can't be earning that much!");
      } else if (typeof value !== 'number') {
        throw new Error("please enter a number")
      }
      this._salary = value;
    } catch (e) {
      console.error(`Error trying to assign salary! ${e.message}.`);
    }
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  set salary(value) {
    try {
      if (value < 800) {
        throw new Error("Come on man, you can't be earning that little!");
      } else if (value > 8000) {
        throw new Error("Come on man, you can't be earning that much!");
      } else if (typeof value !== 'number') {
        throw new Error("please enter a number")
      }
      this._salary = value;
    } catch (e) {
      console.error(`Error trying to assign name! ${e.message}.`);
    }
  }

  get salary() {
    return this._salary * 3;
  }
}

const bradTraversy = new Programmer('Brad', 15, 2500, ['typescript', 'css']);
const cadMerad = new Programmer('', 38, 900, ['French', 'HTML']);
const howardMoon = new Programmer('Howard Moon', 35, 250, ['Cockney', 'some English']);

console.log(bradTraversy);
console.log(cadMerad);
console.log(howardMoon);
